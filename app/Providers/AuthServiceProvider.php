<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Post' => 'App\Policies\PostPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'admin';
            }
            return false;
        });

        Gate::define('isManager', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'manager';
            }
            return false;
        });

        Gate::define('isContentEditor', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'content-editor';
            }
            return false;
        });

        Gate::define('isUniversityPresident', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'university-president';
            }
            return false;
        });

        Gate::define('isDean', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'dean';
            }
            return false;
        });

        Gate::define('isScientificAgent', function ($user) {
            if($user->hasAnyRole()) {
                return $user->roles->first()->slug == 'scientific-agent';
            }
            return false;
        });
        
    }
}
