<?php

use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class CreateAddPublishPostCollumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Posts', function (Blueprint $table) {
            $table->bigInteger('published')->default(0);
        });

        $user = User::create([
            'name' => 'Manar Habbab',
            'email' => 'manar@gmail.com',
            'password' => Hash::make('12345678'),
            'faculty' => 'ITE'
        ]);

        $role = Role::create([
            'name' => 'Admin',
            'slug' => 'admin'
        ]);

        DB::table('users_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Post', function (Blueprint $table) {
            //
        });
    }
}
