<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Dashboard - Ace Admin</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="{{asset('css/ace-fonts.css')}}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{asset('css/ace.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="../assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{asset('js/ace-extra.js')}}"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="../assets/js/html5shiv.js"></script>
		<script src="../assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
        @include('partials.navbar')

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			@include('partials.sidebar')

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
                    @include('partials.breadcrumbs')
                    

					<!-- /section:basics/content.breadcrumbs -->
					@yield('content')
				</div>
			</div><!-- /.main-content -->
            @include('partials.footer')

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='../assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.js"></script>
		<![endif]-->
		<script src="{{asset('js/jquery-ui.custom.js')}}"></script>
		<script src="{{asset('js/jquery.ui.touch-punch.js')}}"></script>
		<script src="{{asset('js/jquery.easypiechart.js')}}"></script>
		<script src="{{asset('js/jquery.sparkline.js')}}"></script>
		<script src="{{asset('js/flot/jquery.flot.js')}}"></script>
		<script src="{{asset('js/flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('js/flot/jquery.flot.resize.js')}}"></script>

		<!-- ace scripts -->
		<script src="{{asset('js/ace/elements.scroller.js')}}"></script>
		<script src="{{asset('js/ace/elements.colorpicker.js')}}"></script>
		<script src="{{asset('js/ace/elements.fileinput.js')}}"></script>
		<script src="{{asset('js/ace/elements.typeahead.js')}}"></script>
		<script src="{{asset('js/ace/elements.wysiwyg.js')}}"></script>
		<script src="{{asset('js/ace/elements.spinner.js')}}"></script>
		<script src="{{asset('js/ace/elements.treeview.js')}}"></script>
		<script src="{{asset('js/ace/elements.wizard.js')}}"></script>
		<script src="{{asset('js/ace/elements.aside.js')}}"></script>
		<script src="{{asset('js/ace/ace.js')}}"></script>
		<script src="{{asset('js/ace/ace.ajax-content.js')}}"></script>
		<script src="{{asset('js/ace/ace.touch-drag.js')}}"></script>
		<script src="{{asset('js/ace/ace.sidebar.js')}}"></script>
		<script src="{{asset('js/ace/ace.sidebar-scroll-1.js')}}"></script>
		<script src="{{asset('js/ace/ace.submenu-hover.js')}}"></script>
		<script src="{{asset('js/ace/ace.widget-box.js')}}"></script>
		<script src="{{asset('js/ace/ace.settings.js')}}"></script>
		<script src="{{asset('js/ace/ace.settings-rtl.js')}}"></script>
		<script src="{{asset('js/ace/ace.settings-skin.js')}}"></script>
		<script src="{{asset('js/ace/ace.widget-on-reload.js')}}"></script>
		<script src="{{asset('js/ace/ace.searchbox-autocomplete.js')}}"></script>

		<!-- inline scripts related to this page -->
        <script type="text/javascript"src="{{asset('js/admin/custom.js')}}">
			
		</script>

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="{{asset('css/ace.onpage-help.css')}}" />
		<link rel="stylesheet" href="{{asset('js/additionals/themes/sunburst.css')}}" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="{{asset('js/ace/elements.onpage-help.js')}}"></script>
		<script src="{{asset('js/ace/ace.onpage-help.js')}}"></script>
		<script src="{{asset('js/additionals/rainbow.js')}}"></script>
		<script src="{{asset('js/additionals/language/generic.js')}}"></script>
		<script src="{{asset('js/additionals/language/html.js')}}"></script>
		<script src="{{asset('js/additionals/language/css.js')}}"></script>
		<script src="{{asset('js/additionals/language/javascript.js')}}"></script>
	</body>
</html>
